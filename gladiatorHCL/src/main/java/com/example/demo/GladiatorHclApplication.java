package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.*")
public class GladiatorHclApplication {

	public static void main(String[] args) {
		SpringApplication.run(GladiatorHclApplication.class, args);
	}
}
