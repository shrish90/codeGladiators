
package com.gladiator.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "transactions"
})
public class Transaction {

    @JsonProperty("transactions")
    private List<Transaction_> transactions = null;

    @JsonProperty("transactions")
    public List<Transaction_> getTransactions() {
        return transactions;
    }

    @JsonProperty("transactions")
    public void setTransactions(List<Transaction_> transactions) {
        this.transactions = transactions;
    }

}
