package com.gladiator.service;

import java.util.List;
import java.util.Map;

import com.gladiator.model.APIModel;

public interface TransactionService {
	public List<APIModel> getAllTransaction();
	public List<APIModel> getAllTransactionByType(String type);
	public Map<String,String> getAllTransactionAmount(String type);
}
