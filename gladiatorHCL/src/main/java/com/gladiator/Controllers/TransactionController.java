package com.gladiator.Controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gladiator.model.APIModel;
import com.gladiator.service.TransactionService;
import com.gladiator.service.serviceImpl.TransactionServiceImpl;

@RestController
public class TransactionController {
	TransactionService tService= new TransactionServiceImpl();
	@RequestMapping(value= "/allTransactions")
	public ResponseEntity<List<APIModel>> getAllTransaction() {
		List<APIModel> transactions = tService.getAllTransaction();
		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}
	@RequestMapping(value= "/allTransactionsByType/{type}")
	public ResponseEntity<List<APIModel>> getAllTransactionByType(@PathVariable("type") String type) {
		List<APIModel> transactions = tService.getAllTransactionByType(type);
		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/AllTransactionAmount/{typea}")
	public ResponseEntity<Map<String,String>> getAllTransactionAmount(@PathVariable("typea") String type) {//
		//List<APIModel> transactions = tService.getAllTransaction();
		Map<String,String> totalAmount = new HashMap<String,String>();
		totalAmount = tService.getAllTransactionAmount(type);
		return new ResponseEntity<>(totalAmount, HttpStatus.OK);
	}
}
